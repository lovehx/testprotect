
import * as type from './mutation-types.js'

export default {
    //更新用户信息数据 渲染视图
    setUserInfo({commit},data) {
        commit(type.GET_USERINFO, data);
    },
    setTest({commit},data){
        commit(type.GET_TEST,data);
    },
    setKeyboard: (context,data) => {
    	context.commit("setKeyboard",data);
  	},
}
