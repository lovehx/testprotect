import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations.js'
import actions from './actions.js'
import getters from './getters.js'

Vue.use(Vuex);
const state = {
    user:'',//用户信息
    listData:[],//规格列表
    redpackPos:[],//红包位置
    xyBox:[],
    findNum:0,
    todayLevel:'',//今日最高关
    timers:"",
    time:'',
    getCode:'',
    isDefault:true,//是否默认‘福’
    home: {                //首页数据

    },
    personal:{    //个人中心的信息数据
      state:''
    },
    goodsDetail:{//商品详情的信息数据

    },
    address:{

    },
    keyboard:''
	
};

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
