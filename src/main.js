// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import vuex from 'vuex'
import store from './store'
import FastClick from 'fastclick'
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css';
import vfoot from "./components";//全局组件
import * as filters from './filters' //全局过滤器
Vue.use(vfoot);

Vue.use(vuex);

Object.keys(filters).forEach(key=>{
	console.log(key)
	Vue.filter(key,filters[key])
})

Vue.config.productionTip = false;

FastClick.attach(document.body);

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store,
    render: h => h(App)
})
