import config from '@/config';
export default async(url='',data={},type='GET')=>{
    type=type.toUpperCase();
    url=config.host+url;
    if(type=='GET'){
        let dataStr='';
        Object.keys(data).forEach(key=>{
            dataStr+=key+'='+data[key]+'&';
        })

        if(dataStr!=''){
            dataStr=dataStr.substr(0,dataStr.lastIndexOf('&'));
            url= url + '?' + dataStr;
        }
    }
    return new Promise((resolve,reject) => {
        let  requestObj;
        if(window.XMLHttpRequest){
            requestObj=new XMLHttpRequest();
        }else {
            requestObj=new ActiveXObject();
        }
        let sendData='';
        requestObj.open(type,url,true);
        requestObj.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        requestObj.send(sendData);
        requestObj.onreadystatechange=()=>{
            if(requestObj.readyState==4){
                if(requestObj.status==200){
                    let obj=requestObj.response;
                    if(typeof obj !=='object'){
                        obj=JSON.parse(obj);
                    }
                    resolve(obj)
                }else {
                    reject(requestObj)
                }
            }
        }

    } )

}