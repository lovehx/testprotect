import fetch from './fetch.js'
export const logins=data=>fetch('/user/login',data);

export const getConfigList=data=>fetch('/jsdn/getConfigList',data);
export const getPlayQuestion=data=>fetch('/jsdn/getPlayQuestion',data);

export const personalTop=data=>fetch('/jsdn/personalTop',data); //个人排行
export const groupTop=data=>fetch('/jsdn/groupTop',data); //团队排行
export const selectLaunchList=data=>fetch('/jsdn/selectLaunchList',data); //我发起的挑战
export const joinList=data=>fetch('/jsdn/joinList',data); //我参与的挑战  
export const createGroup=data=>fetch('/jsdn/createGroup',data); //创建团队
export const myGroup=data=>fetch('/jsdn/myGroup',data); //我的团队
export const getQuestion=data=>fetch('/jsdn/getQuestion',data); //挑战赛获取题目
export const bingGo=data=>fetch('/jsdn/bingGo',data); //挑战赛答题正确保存  num表示第几题由题目那里获得
export const receiveAward=data=>fetch('/jsdn/receiveAward',data); //挑战赛完成 保存最终得分 即最后一题答错后需要调的
export const launchDetail=data=>fetch('/jsdn/launchDetail',data); //获取发起详情

export const start=data=>fetch('/jsdn/start',data); //开始 用金币开始挑战
export const personalInfo=data=>fetch('/jsdn/personalInfo',data); //首页信息
export const submit=data=>fetch('/trade/submit',data); //下单
export const launch=data=>fetch('/jsdn/launch',data); //余额下单
export const pass=data=>fetch('/jsdn/pass',data); //金币过关 只消耗金币
export const getMoneyChange=data=>fetch('/portal/redpackAccount/getMoneyChange',data); //获取钱包明细
export const otherLogin=data=>fetch('/portal/redpackUser/otherLogin',data); //注册

export const getPersonalInfo=data=>fetch('/portal/redpackAccount/getPersonalInfo',data); //用户信息
//提现

export const getPayWayNew=data=>fetch('/portal/redpackAccount/getPayWayNew',data); //提现类型
export const toBackAccount=data=>fetch('/portal/redpackAccount/toBackAccount',data); //提现

export const phoneVerifyCodeByTemplate=data=>fetch('/portal/redpackAccount/phoneVerifyCodeByTemplate',data); //获取新的验证码

export const alipayBand=data=>fetch('/portal/redpackAccount/alipayBand',data); 
export const getAliaAccount=data=>fetch('/portal/redpackAccount/getAliaAccount',data); //记录提现帐号

export const getPaymentCode=data=>fetch('/trade/getPaymentCode',data); //获取收款码	
export const getLastFailDraw=data=>fetch('/portal/redpackAccount/getLastFailDraw',data); //提现失败引导接口	
export const paymentUpload=data=>fetch('/trade/paymentUpload',data); //上传二维码
export const withDraw=data=>fetch('/trade/withDraw',data); //上传二维码	

export const phoneVerifyCode=data=>fetch('/portal/redpackAccount/phoneVerifyCode',data); //获取验证码
export const bindPhones=data=>fetch('/portal/redpackAccount/phoneBand',data); //绑定手机号
export const checkphonePass=data=>fetch('/portal/redpackAccount/phonePass',data); //验证秘钥
export const getPhoneBand=data=>fetch('/portal/redpackAccount/getPhoneBand',data); //是否绑定
export const checkVerifyCode=data=>fetch('/portal/redpackAccount/checkVerifyCode',data); //验证验证码
export const findPhoneBand=data=>fetch('/portal/redpackAccount/findPhoneBand',data); //获取手机号

  