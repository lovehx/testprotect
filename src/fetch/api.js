import axios from 'axios'
import config from '../config'

// axios 配置
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

axios.interceptors.response.use(function(response){
    console.log(response.data);
    return response;
},function(error){
    //对返回的错误进行一些处理
    return Promise.reject(error);
});
export default {
   login(openId){
      return axios.get(config.ip.user+'/login', {
        params: {
          openId: openId,
          loginType: 2,
        }
      });
    },
  //选择城市
  selectRegion(type,id){
    return axios.get('http://192.168.1.11:8080/portal/address/selectRegion', {
      params: {
        type: type,
        id:id
      }
    });
  },
    //提现类型
    getPayWayNew(token){
        return axios.get('http://m.wlwch.cn/portal/redpackAccount/getPayWayNew', {
            params: {
                token: token
            }
        });
    },
    getCouponCount(token){
        return axios.get('http://m.wlwch.cn/portal/coupon/getCouponCount',{
            params: {
                token: token
            }
        })
    },
    getRankingList(token){
        return axios.get('http://m.wlwch.cn/portal/rich/getRankingList',{
            params: {
                token: token
            }
        })
    },
    inviteMoney(token){
        return axios.get('http://m.wlwch.cn/portal/inviteMoney/takeInviteCache',{
            params: {
                token: token
            }
        })
    },
    newMyInvite(token){
        return axios.get('http://m.wlwch.cn/portal/inviteMoney/myInvite',{
            params: {
                token: token
            }
        })
    },
    takeInviteCache(token){
        return axios.get('http://m.wlwch.cn/portal/levelInvite/takeInviteCache',{
            params: {
                token: token
            }
        })
    },
    myEarn(token){
        return axios.get('http://m.wlwch.cn/portal/levelInvite/myEarn',{
            params: {
                token: token
            }
        })
    },
}


