import common from '@/assets/js/common';
var mixin={
    data(){
        return{
            user:common.getJsonLocal('user'),
        }
    },
    methods:{
        showMd: function (md) {
            this.md[md] = true;
            this.md.mask = true;
        },
        closeMd: function (md) {
            this.md[md] = false;
            this.md.mask = false;
        },
        closeAllMd: function (md) {
            for(let item in md){
                this.md[item] = false;
                this.md.mask = false;
            }
        },
        forwardDirection(){
            for(var z=0;z<this.$store.state.listData.length;z++){
                this.$store.state.listData[z].startActive=true;//是否开始旋转
                this.$store.state.listData[z].endActive=false;//是否翻转
                this.$set(this.$store.state.listData,z,this.$store.state.listData[z]);
            }
            setTimeout(()=>{
                for(var p=0;p<this.$store.state.listData.length;p++){
                    this.$store.state.listData[p].isDefault=false;
                    this.$set(this.$store.state.listData,p,this.$store.state.listData[p]);
                }
            },300);



        },
        backDirection(){
            setTimeout(()=>{
                for(var z=0;z<this.$store.state.listData.length;z++){
                    this.$store.state.listData[z].startActive=false;//是否开始旋转
                    this.$store.state.listData[z].endActive=true;//是否翻转
                    this.$set(this.$store.state.listData,z,this.$store.state.listData[z]);
                }
                setTimeout(()=>{
                    for(var p=0;p<this.$store.state.listData.length;p++){
                        this.$store.state.listData[p].isDefault=true;
                        this.$set(this.$store.state.listData,p,this.$store.state.listData[p]);
                    }
                    this.btnText='结束挑战';
                },300);
            },1000);
        }
    },
    created(){
        this.probeType = 3; // scroll组件需要实施派发scroll事件，且支持swipe
        this.listenScroll = true; // scroll组件监听scroll事件并派发滚动位置
    },
};
export default mixin