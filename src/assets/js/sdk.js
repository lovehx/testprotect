import common from '@/assets/js/common';
import {otherLogin} from "../../fetch/index";
let sdk={
    data(){
        return{
            channel:'',
            ids:['donation','donation6']
        }
    },
    methods:{
        wxLogin(){//微信登录
            let vm=this;
            let s = authsp[0];
            if ( !s.authResult ) {
                s.login( function(e){
                    vm.authUserInfo();
                }, function(e){
                    alert( "登录认证失败！" );
                } );
            }else{
                //alert( "已经登录认证！" );
                vm.authUserInfo();
            }
        },
        authUserInfo(){//获取用户信息
            let vm=this;
            let s = authsp[0];
            if ( !s.authResult ) {
                alert("未登录授权！");
            } else {
                s.getUserInfo( function(e){
                    let wx_unionid = e.target.userInfo.unionid;
                    let wx_openid = s.userInfo.openid;
                    let wx_nickname = s.userInfo.nickname;
                    let wx_sex = s.userInfo.sex;
                    let wx_country = s.userInfo.country;
                    let wx_city = s.userInfo.city;
                    let wx_province = s.userInfo.province;
                    let wx_headimgurl = s.userInfo.headimgurl;
                    localStorage.setItem("openid",wx_openid);

                    vm.sendUser(wx_unionid,wx_openid,wx_nickname,wx_sex,wx_country,wx_city,wx_province,wx_headimgurl,wx_channel,wx_imei);
                }, function(e){
                    alert( "获取用户信息失败："+e.message+" - "+e.code );
                } );
            }
        },
        sendUser(wx_unionid,wx_openid,wx_nickname,wx_sex,wx_country,wx_city,wx_province,wx_headimgurl,wx_channel,wx_imei){//存储用户信息

            otherLogin({unionid:wx_unionid,openid:wx_openid,name:wx_nickname,sex:wx_sex,country:wx_country,city:wx_city,province:wx_province,headimgurl:wx_headimgurl,channel:wx_channel,imei:wx_imei}).then((res) => {
                if(res.msgCode == "200") {//注册
                    location.reload();//刷新
                    //alert('成功');
                }else {
                    alert('登录失败');
                }
            })

        },
        shareWeixinMessage(content,url,title,pic){//分享到微信
            sharewx.send( {content:content,href:url,title:title,pictures:pic,extra:{scene:"WXSceneSession"}}, function(){
                alert( "分享成功！" );
            }, function(e){
                alert( "分享失败："+e.message );
            });
        },
        chooseImg(){//相册选取
            let vm=this;
            let actionbuttons=[{title:"相册选取"}];
            let actionstyle={title:"选择照片",cancel:"取消",buttons:actionbuttons};
            plus.nativeUI.actionSheet(actionstyle, function(e){
                if(e.index==1){
                    vm.appendByGallery();
                }
            } );
        },
        appendByGallery(){//获取路径
            let vm=this;
            plus.gallery.pick(function(path){
                vm.createUpload(path)
            });
        },
        createUpload(path){//上传到服务器
            let vm=this;
            let task = plus.uploader.createUpload( "http://ws.wlwch.cn/customer/imageUpload",
                { method:"POST",blocksize:2048000,priority:100 },
                function ( t, status ) {
                    //
                    // 上传完成
                    if ( status == 200 ) {
                        //alert(JSON.parse(t.responseText).imagePath);
                        vm.successUpload(JSON.parse(t.responseText).imagePath);//上传成功的回调

                    } else {
                        alert( "Upload failed: " + status );
                    }
                }
            );

            task.addFile(path, {key:'file'});
            task.addData("token", common.getJsonLocal('user').token);
            task.start();
        },
        allPay(channel,data){//支付宝 微信 支付
            //alert(channel)
            plus.payment.request(channel,data,function(result){ //后台返回的data
                plus.nativeUI.alert("支付成功！",function(){
                    location.reload();
                    back();
                });
            },function(error){
                //plus.nativeUI.alert("支付失败：" + error.code);
                plus.nativeUI.alert("支付失败");
                location.reload();
            });
        }

    },
    created(){
        let vm=this;


        /**********************************************************************/

    },
};
export default sdk