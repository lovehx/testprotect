import Vue from 'vue'
import Router from 'vue-router'

const home = r => require.ensure([], () => r(require('@/pages/home/home')), 'home');
const friendChallenge = r => require.ensure([], () => r(require('@/pages/home/friendChallenge')), 'friendChallenge');
const challengeRound = r => require.ensure([], () => r(require('@/pages/challengeRound/index')), 'challengeRound');
const myChallenge = r => require.ensure([], () => r(require('@/pages/home/myChallenge')), 'myChallenge');
const wallet = r => require.ensure([], () => r(require('@/pages/wallet/index')), 'wallet');
const playGame = r => require.ensure([], () => r(require('@/pages/challengeRound/playGame')), 'playGame');

const draw = r => require.ensure([], () => r(require('@/pages/draw/draw')), 'draw');
const purseDetail = r => require.ensure([], () => r(require('@/pages/wallet/purseDetail')), 'wallet');
const redResults = r => require.ensure([], () => r(require('@/pages/home/redResults')), 'home');
const giveDetails = r => require.ensure([], () => r(require('@/pages/wallet/giveDetails')), 'wallet');
const commonProblem = r => require.ensure([], () => r(require('@/pages/home/commonProblem')), 'home');
const myTeam = r => require.ensure([], () => r(require('@/pages/home/myTeam')), 'home');
const tryPlay = r => require.ensure([], () => r(require('@/pages/home/tryPlay')), 'tryPlay');
const friendTrial = r => require.ensure([], () => r(require('@/pages/home/friendTrial')), 'home');

//提现
const bindPhone = r => require.ensure([], () => r(require('@/pages/draw/bindPhone')), 'bindPhone');
const findPassword = r => require.ensure([], () => r(require('@/pages/draw/findPassword')), 'findPassword');

const myPerson = r => require.ensure([], () => r(require('@/pages/home/myPerson')), 'home');
const toList = r => require.ensure([], () => r(require('@/pages/challengeRound/toList')), 'challengeRound');
const personalMsg = r => require.ensure([], () => r(require('@/pages/home/personalMsg')), 'personalMsg');
const test = r => require.ensure([], () => r(require('@/pages/home/test')), 'personalMsg');


Vue.use(Router);

const router = new Router({
    routes: [
        {
        	path: '/',
            name: 'home',
            component: home,
            meta:{
                keepAlive:true
            }
        },
        {
            path: '/home',
            name: 'home',
            component: home,
            meta:{
                keepAlive:true
            }
        },
        {
            path: '/friendChallenge/:launchNo',
            name: 'friendChallenge',
            component: friendChallenge
        },
        {
            path: '/challengeRound',
            name: 'challengeRound',
            component: challengeRound,
            meta:{
                keepAlive:true
            }
        },
        
        {
            path: '/myChallenge',
            name: 'myChallenge',
            component: myChallenge
        },
        {
            path: '/wallet',
            name: 'wallet',
            component: wallet
        },
        {
            path: '/playGame',
            name: 'playGame',
            component: playGame
        },
        {
            path: '/draw/:menu',
            name: 'draw',
            component: draw
        },
        
        {
            path: '/purseDetail',
            name: 'purseDetail',
            component: purseDetail
        },
        {
            path: '/redResults/:launchNo',
            name: 'redResults',
            component: redResults
        },
        {
            path: '/giveDetails',
            name: 'giveDetails',
            component: giveDetails
        },
        
        {
            path: '/commonProblem',
            name: 'commonProblem',
            component: commonProblem
        },
        
        {
            path: '/myTeam',
            name: 'myTeam',
            component: myTeam
        },
        {
            path: '/tryPlay',
            name: 'tryPlay',
            component: tryPlay
        },
        {
            path: '/friendTrial',
            name: 'friendTrial',
            component: friendTrial,
            meta:{
                keepAlive:true
            },
        },
    // 提现
        {
            path: '/bindPhone',
            name: 'bindPhone',
            component: bindPhone
        },
        {
            path: '/findPassword',
            name: 'findPassword',
            component: findPassword
        },
        {
            path: '/myPerson',
            name: 'myPerson',
            component: myPerson
        },
        
        {
            path: '/toList',
            name: 'toList',
            component: toList
        },
        {
            path: '/personalMsg',
            name: 'personalMsg',
            component: personalMsg
        },
        {
            path: '/test',
            name: 'test',
            component: test
        }
        
    ]
})

// 判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
    if (to.matched.some(res => res.meta.requireAuth)) { // 判断是否需要登录权限
        if (localStorage.getItem('openid')) { // 判断是否登录
            next()
        } else { // 没登录则跳转到登录界面
            next({
                path: '/home',
                query: {redirect: to.fullPath}
            });
            window.location.reload()
        }
    } else {
        next()
    }
});
export default router